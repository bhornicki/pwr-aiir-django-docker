# Introduction
WUST "Aplikacje Internetowe i Rozproszone" (internet and distributed applications) course project, under Mateusz Gniewkowski supervision.

# Objective
Design distributed computing solver of Traveling Salesman Problem with web interface. 
# Technologies
- database: MySQL
- backend: Python + Django
- cluster task queue: RabbitMQ
- Frontend: ReactJS (Grzegorz part, but he abandoned the project)
- Computing cluster: C++ and MPI (Piotr and Szymon part), started up by Python script

All architecture parts are in separate docker containers.
# Team
 - Piotr Florkiewicz
 - Bartosz Hornicki
 - Szymon Pleśnierowicz

# Starting entire project
Use 'docker-compose up' command in project root folder.
